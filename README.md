# IR - Interactive Rename

`ir` - rename files in place, making it easy to adjust long filenames

#### Features:
+ Like `mv`, overwrites files without notice
+ Unlike most `mv` programs, does not rename files across filesystems
+ Uses `readline`
+ Basic filename tab-completion support

#### Usage:
`ir <files...>`

### Demo:
![Demo](https://raw.githubusercontent.com/vojtechkral/irename/master/demo.gif)
